function generateChart (cse,msc,ee,bsbe,me) {
     var chart = c3.generate({
          bindto: document.getElementById('chart1'),
         data: {
         columns: [
               ['CSE',cse],
               ['MSC',msc],
               ['EE',ee],
               ['BSBE',bsbe],
               ['ME',me],
              ],
              type: 'bar'
         }
     });
};

function ChartHandler () {
     var cse = 4;
     var msc = 3;
     var ee = 2;
     var bsbe = 1;
     var me  = 4;   
     generateChart (cse,msc,ee,bsbe,me);
};

ChartHandler();