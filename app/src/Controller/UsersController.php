<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Core\App;

App::import(
    'Vendor',
    'Highchart',
    array('file' => 'ghunti' . DS . 'Highchart.php')
);


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    // User login
    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect(['action' => 'profile']);
            }
            // Error in case of login failed
            $this->Flash->error('Invalid Username or Password');
        }
    }

     // Logout
    public function logout(){
         $this->Flash->success('logged out successfully');
         return $this->redirect($this->Auth->logout());
    }

    public function register(){
        $user = $this->Users->newEntity();
        if($this->request->is('post')){
            $user = $this->Users->patchEntity($user, $this->request->data);
            if($this->Users->save($user)){
                $this->Flash->success('You are registered and can login');
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error('You are not registered');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialzie', ['user']);
    }
    
    public function profile(){
        $id = $this->Auth->user('id');
        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['id' => $id]);
        $this->set(compact('query'));

    }

    public function profile(){
        $id = $this->Auth->user('id');
        $users = TableRegistry::get('Users');
        $query = $users->find()->where(['id' => $id]);
        $this->set(compact('query'));

    }

    public function chart1(){
        $result =$this->Users->query("select department from users;");
        $count = array('CSE'=>0,'ME'=>0,'BSBE'=>0);
        foreach ($result as $key ) {
           $count[$key['department']]++;
           //echo $key['department'];
        }
        $this->set(compact('count'));
        //print_r($count);

        $chart = new Highchart();
        $chart->chart = array(
            'renderTo' => 'chart1', // div ID where to render chart
            'type' => 'line'
        );

        $chart->series[0]->name = 'Tokyo';
        $chart->series[0]->data = array(7.0, 6.9, 9.5);
        $this->set( compact('chart'));
    }

    // public function tag()
    // {
    // // The 'pass' key is provided by CakePHP and contains all
    // // the passed URL path segments in the request.
    // $tag = $this->request->params['pass'];
    // // Use the BookmarksTable to find tagged bookmarks.
    // $user = $this->Users->find('tagged', [
    // 'name' => $tag
    // ]);
    // // Pass variables into the view template context.
    // $this->set([
    // 'users' => $user,
    // 'name' => $tag
    // ]);
    // }

    public function beforeFilter(Event $event){
        $this->Auth->allow(['register']);
    }

}
